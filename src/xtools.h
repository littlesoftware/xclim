#pragma once

#include <string>
#include <stdint.h>

namespace xtools
{
  
std::string atomToString( const uint64_t atom );
std::string getAtomName( const uint64_t atom, void* display );
bool isAtomType( const uint64_t atom );
bool isIntegerType( const uint64_t atom );
uint64_t getAtomByName( const std::string& name, void* display );
size_t getItemSize( int format );

} // namespace xtools
