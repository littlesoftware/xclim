#pragma once

#include "IClient.h"
#include "types.h"

#include <X11/Xlib.h>

#include <string>
#include <vector>

class Xclient
{
public:
  Xclient();
  ~Xclient();

  void open();
  void close();
  void setInterface( IClient* interface );
  void setFormated( const bool formated = true );
  void setDumped( const bool dumped = true );

  void getList();
  void getTarget( const std::string& name );
  void parseDump();
  void bind();

private:
  void selection( const char* name );
  void finish();
  void onAtom();
  void onIncr();
  void printFormat();

  void onSelectionRequest( const XEvent& event );

  Bytes readAtoms( const void* data, size_t size ) const;
  Bytes readIntegers( const void* data, size_t size ) const;
  
  Atom getSelection();
  Atom getClipboardAtom();
  Atom getTargetsAtom();
  Atom getXclimAtom();
  Atom getIncrAtom();

private:
  IClient* m_interface;

  Display* m_display;
  Window m_root;
  Window m_window;

  Atom m_type;
  int m_format;
  unsigned long m_bytes;
  unsigned long m_size;
  unsigned long m_nitems;
  std::string m_name;

  Atom m_selection;
  Atom m_clipboardAtom;
  Atom m_targetsAtom;
  Atom m_xclimAtom;
  Atom m_incrAtom;

  bool m_formatedFlag;
  bool m_dumpedFlag;
  char m_endl;
};

