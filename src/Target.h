#pragma once

#include "types.h"

#include <X11/Xdefs.h>
#include <optional>
#include <string>
#include <map>
#include <stdio.h>
#include <variant>
#include <list>

class Target
{
public:
  Target();
  Target( const Target& obj ) = delete;
  Target( Target&& obj );
  ~Target();

  void setName( const std::string& name );
  const std::string& getName() const;

  void setFormat( const std::string& format );
  const std::string& getFormat() const;

  void setData( const void* data, const size_t size );
  void setFile( const char* filename );
  void storeInMemory();
  void storeInCache();

  void setDump();
  bool isDump() const;

  bool isReference() const;
  void setReference( const std::string& target );
  const std::string& getReference() const;

  void openIn();
  void openOut();
  void close();
  void saveIn( const size_t limit );
  void saveIn( const void* data, const size_t size, const size_t limit );

  size_t read( void* data, const size_t pos, const size_t size ) const;
  size_t getSize() const;

  void reset();

  void write( const void* data, const size_t size );

private:
  std::string m_name;
  std::string m_format;
  Bytes m_data;
  std::string m_filename;
  FILE* m_file;
  FILE* m_cache;
  bool m_storeInMemory;
  bool m_storeInCache;
  bool m_isDump;
  size_t m_size;
  bool m_storeInReference;
  std::string m_reference;

  //std::optional< FILE* > m_ref;
};
