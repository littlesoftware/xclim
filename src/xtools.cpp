#include "xtools.h"

#include <X11/Xdefs.h>
#include <X11/Xatom.h>
#include <X11/Xlib.h>

#include <map>
#include <sstream>

namespace xtools
{

std::string atomToString( const uint64_t atom )
{
  static std::map< uint64_t, std::string > dict = {
    { XA_PRIMARY, "XA_PRIMARY" },
    { XA_SECONDARY, "XA_SECONDARY" },
    { XA_ARC, "XA_ARC" },
    { XA_ATOM, "XA_ATOM" },
    { XA_BITMAP, "XA_BITMAP" },
    { XA_CARDINAL, "XA_CARDINAL" },
    { XA_COLORMAP, "XA_COLORMAP" },
    { XA_CURSOR, "XA_CURSOR" },
    { XA_CUT_BUFFER0, "XA_CUT_BUFFER0" },
    { XA_CUT_BUFFER1, "XA_CUT_BUFFER1" },
    { XA_CUT_BUFFER2, "XA_CUT_BUFFER2" },
    { XA_CUT_BUFFER3, "XA_CUT_BUFFER3" },
    { XA_CUT_BUFFER4, "XA_CUT_BUFFER4" },
    { XA_CUT_BUFFER5, "XA_CUT_BUFFER5" },
    { XA_CUT_BUFFER6, "XA_CUT_BUFFER6" },
    { XA_CUT_BUFFER7, "XA_CUT_BUFFER7" },
    { XA_DRAWABLE, "XA_DRAWABLE" },
    { XA_FONT, "XA_FONT" },
    { XA_INTEGER, "XA_INTEGER" },
    { XA_PIXMAP, "XA_PIXMAP" },
    { XA_POINT, "XA_POINT" },
    { XA_RECTANGLE, "XA_RECTANGLE" },
    { XA_RESOURCE_MANAGER, "XA_RESOURCE_MANAGER" },
    { XA_RGB_COLOR_MAP, "XA_RGB_COLOR_MAP" },
    { XA_RGB_BEST_MAP, "XA_RGB_BEST_MAP" },
    { XA_RGB_BLUE_MAP, "XA_RGB_BLUE_MAP" },
    { XA_RGB_DEFAULT_MAP, "XA_RGB_DEFAULT_MAP" },
    { XA_RGB_GRAY_MAP, "XA_RGB_GRAY_MAP" },
    { XA_RGB_GREEN_MAP, "XA_RGB_GREEN_MAP" },
    { XA_RGB_RED_MAP, "XA_RGB_RED_MAP" },
    { XA_STRING, "XA_STRING" },
    { XA_VISUALID, "XA_VISUALID" },
    { XA_WINDOW, "XA_WINDOW" },
    { XA_WM_COMMAND, "XA_WM_COMMAND" },
    { XA_WM_HINTS, "XA_WM_HINTS" },
    { XA_WM_CLIENT_MACHINE, "XA_WM_CLIENT_MACHINE" },
    { XA_WM_ICON_NAME, "XA_WM_ICON_NAME" },
    { XA_WM_ICON_SIZE, "XA_WM_ICON_SIZE" },
    { XA_WM_NAME, "XA_WM_NAME" },
    { XA_WM_NORMAL_HINTS, "XA_WM_NORMAL_HINTS" },
    { XA_WM_SIZE_HINTS, "XA_WM_SIZE_HINTS" },
    { XA_WM_ZOOM_HINTS, "XA_WM_ZOOM_HINTS" },
    { XA_MIN_SPACE, "XA_MIN_SPACE" },
    { XA_NORM_SPACE, "XA_NORM_SPACE" },
    { XA_MAX_SPACE, "XA_MAX_SPACE" },
    { XA_END_SPACE, "XA_END_SPACE" },
    { XA_SUPERSCRIPT_X, "XA_SUPERSCRIPT_X" },
    { XA_SUPERSCRIPT_Y, "XA_SUPERSCRIPT_Y" },
    { XA_SUBSCRIPT_X, "XA_SUBSCRIPT_X" },
    { XA_SUBSCRIPT_Y, "XA_SUBSCRIPT_Y" },
    { XA_UNDERLINE_POSITION, "XA_UNDERLINE_POSITION" },
    { XA_UNDERLINE_THICKNESS, "XA_UNDERLINE_THICKNESS" },
    { XA_STRIKEOUT_ASCENT, "XA_STRIKEOUT_ASCENT" },
    { XA_STRIKEOUT_DESCENT, "XA_STRIKEOUT_DESCENT" },
    { XA_ITALIC_ANGLE, "XA_ITALIC_ANGLE" },
    { XA_X_HEIGHT, "XA_X_HEIGHT" },
    { XA_QUAD_WIDTH, "XA_QUAD_WIDTH" },
    { XA_WEIGHT, "XA_WEIGHT" },
    { XA_POINT_SIZE, "XA_POINT_SIZE" },
    { XA_RESOLUTION, "XA_RESOLUTION" },
    { XA_COPYRIGHT, "XA_COPYRIGHT" },
    { XA_NOTICE, "XA_NOTICE" },
    { XA_FONT_NAME, "XA_FONT_NAME" },
    { XA_FAMILY_NAME, "XA_FAMILY_NAME" },
    { XA_FULL_NAME, "XA_FULL_NAME" },
    { XA_CAP_HEIGHT, "XA_CAP_HEIGHT" },
    { XA_WM_CLASS, "XA_WM_CLASS" },
    { XA_WM_TRANSIENT_FOR, "XA_WM_TRANSIENT_FOR" }
  };
  std::stringstream os;
  os << atom;
  if( atom >= 1 && atom <= XA_LAST_PREDEFINED )
  {
    os << " (" << dict[ atom ] << ")";
  }
  return os.str();
}

std::string getAtomName( const uint64_t atom, void* display )
{
  std::string result;
  char* name = XGetAtomName( static_cast< Display* >( display ), atom );
  result = name;
  XFree( name );
  return result;
}

bool isAtomType( const uint64_t atom )
{
  return atom == XA_ATOM;
}

bool isIntegerType( const uint64_t atom )
{
  return atom == XA_INTEGER;
}

uint64_t getAtomByName( const std::string& name, void* display )
{
  return XInternAtom( static_cast< Display* >( display ), name.c_str(), False );
}

size_t getItemSize( int format )
{
  switch( format )
  {
  case 8: return sizeof( char );
  case 16: return sizeof( short );
  case 32: return sizeof( long );
  default: return 0;
  }
}

} // namespace xtools
