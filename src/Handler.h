#pragma once

#include "Manager.h"

#include <map>
#include <string>
#include <memory>

class Target;

class Handler
{
public:
  enum class Action
  {
    Unknown,
    Help,
    Version,
    Copy,
    Paste,
    List
  };

  Handler();
  ~Handler();

  static int execute( int argc, char** argv );
  static Handler& instance();

  void parseOptions( int argc, char** argv );
  Action getAction() const;

private:
  void onHelp();
  void onVersion();
  void onCopy();
  void onPaste();
  void onList();

private:
  using TargetPtr = std::unique_ptr< Target >;
  using TargetList = std::map< std::string, TargetPtr >;

  static const char* Targets;

  Action m_action;
  Manager m_manager;
};
