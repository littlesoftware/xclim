#include "XBase.h"

#include <stdexcept>

XBase::XBase()
{
}

XBase::~XBase()
{
}

void XBase::open()
{
  if( !m_display )
  {
    m_display = XOpenDisplay( nullptr );
    if( !m_display )
      throw std::logic_error( "Cannot open the display" );
  }
  m_root = XDefaultRootWindow( m_display );
  m_window = XCreateSimpleWindow( m_display, m_root, 0, 0, 1, 1, 0, 0, 0 );
}

void XBase::close()
{
  if( m_display )
  {
    XCloseDisplay( m_display );
    m_display = nullptr;
  }
}
