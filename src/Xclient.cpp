#include "Xclient.h"
#include "log.h"
#include "xtools.h"
#include "tools.h"

#include <X11/Xatom.h>
#include <X11/Xlib.h>
#include <X11/extensions/Xfixes.h>

#include <stdexcept>
#include <map>
#include <list>
#include <sstream>
#include <cstring>

const char *evtstr[ LASTEvent ] = {
    "ProtocolError",  "ProtocolReply",  "KeyPress",         "KeyRelease",
    "ButtonPress",    "ButtonRelease",  "MotionNotify",     "EnterNotify",
    "LeaveNotify",    "FocusIn",        "FocusOut",         "KeymapNotify",
    "Expose",         "GraphicsExpose", "NoExpose",         "VisibilityNotify",
    "CreateNotify",   "DestroyNotify",  "UnmapNotify",      "MapNotify",
    "MapRequest",     "ReparentNotify", "ConfigureNotify",  "ConfigureRequest",
    "GravityNotify",  "ResizeRequest",  "CirculateNotify",  "CirculateRequest",
    "PropertyNotify", "SelectionClear", "SelectionRequest", "SelectionNotify",
    "ColormapNotify", "ClientMessage",  "MappingNotify",    "GenericEvent",
};

Xclient::Xclient():
  m_interface( nullptr ),
  m_display( nullptr ),
  m_selection( None ),
  m_clipboardAtom( None ),
  m_targetsAtom( None ),
  m_xclimAtom( None ),
  m_incrAtom( None ),
  m_formatedFlag( false ),
  m_dumpedFlag( false ),
  m_endl( '\n' )
{
}

Xclient::~Xclient()
{
  close();
}

void Xclient::open()
{
  if( !m_display )
  {
    m_display = XOpenDisplay( nullptr );
    if( !m_display )
      throw std::logic_error( "Cannot open the display" );
  }
  m_root = XDefaultRootWindow( m_display );
  m_window = XCreateSimpleWindow( m_display, m_root, 0, 0, 1, 1, 0, 0, 0 );
}

void Xclient::close()
{
  if( m_display )
  {
    XCloseDisplay( m_display );
    m_display = nullptr;
  }
}

void Xclient::setInterface( IClient* interface )
{
  m_interface = interface;
}

void Xclient::setFormated( const bool formated )
{
  m_formatedFlag = formated;
}

void Xclient::setDumped( const bool dumped )
{
  m_dumpedFlag = dumped;
  m_endl = dumped ? '\0' : '\n';
}

void Xclient::getList()
{
  getTarget( "TARGETS" );

  auto sz = XExtendedMaxRequestSize( m_display );
  LOG_DEBUGF( "ext max = %ld", sz );
  auto sz2 = XMaxRequestSize( m_display );
  LOG_DEBUGF( "max = %ld", sz2 );
}

void Xclient::getTarget( const std::string& name )
{
  m_name = name;
  // selection
  selection( name.c_str() );
  // get resource
  if( m_type == getIncrAtom() )
  {
    finish();
    // do INCR
    onIncr();
  }
  else
  {
    // print atom
    onAtom();
    finish();
  }
}

void Xclient::parseDump()
{
  const size_t size = 1024;
  size_t pos = 0;
  uint8_t buffer[ size ];
  std::string name;
  std::string format;
  void* data;
  size_t readed = 0;

  while( true )
  {
    const auto bytes = m_interface->onReadDump( buffer, size, pos );
    if( bytes <= 0 )
      break;
    pos += bytes;
  }
}

void Xclient::bind()
{
  LOG_POINT;

  // selection
  Atom selection = getSelection();
  XSetSelectionOwner( m_display, selection, m_window, CurrentTime );

  // check owner
  Window owner = XGetSelectionOwner( m_display, selection );
  if( owner != m_window )
  {
    LOG_DEBUG( "Window owner is not correct" );
    return;
  }

  // loop events
  bool loop = true;
  while( loop )
  {
    XEvent event;
    XNextEvent( m_display, &event );
    LOG_DEBUGF( "event: %s", evtstr[ event.type ] );

    switch( event.type )
    {
    case SelectionRequest:
      onSelectionRequest( event );
      break;
    case PropertyNotify:
      break;
    case SelectionClear:
      loop = false;
      break;
    default:
      LOG_DEBUG( "event: ignored" );
      break;
    }
  }
}

void Xclient::selection( const char* name )
{
  XEvent event;
  int format;
  unsigned long nitems;
  unsigned char* properties = nullptr;
  m_type = None;
  // get atom
  Atom target = XInternAtom( m_display, name, False );
  // make selection
  XConvertSelection(
    m_display,          // display
    getSelection(),     // selection
    target,             // target
    getXclimAtom(),     // property
    m_window,           // requestor
    CurrentTime         // time
  );
  // wait Selection Notify
  while( true )
  {
    XNextEvent( m_display, &event );
    if( event.type == SelectionNotify )
      break;
  }
  // Check by property
  if( event.xselection.property == None )
  {
    throw std::logic_error( "Conversion could not be performed" );
  }
  // get properies
  XGetWindowProperty(
    m_display,       // display 
    m_window,        // window
    getXclimAtom(),  // property 
    0,               // long_offset 
    0,               // long_length 
    False,           // delete 
    AnyPropertyType, // req_type 
    &m_type,         // actual_type_return 
    &format,         // actual_format_return 
    &nitems,         // nitems_return 
    &m_bytes,        // bytes_after_return 
    &properties      // prop_return
  );
  XFree( properties );
}

void Xclient::finish()
{
  XDeleteProperty( m_display, m_window, getXclimAtom() );
}

void Xclient::onAtom()
{
  Atom type = None;
  int format;
  unsigned long size;
  unsigned char* properties = nullptr;

  XGetWindowProperty(
    m_display,       // display 
    m_window,        // window
    getXclimAtom(),  // property 
    0,               // long_offset 
    m_bytes,         // long_length 
    False,           // delete 
    AnyPropertyType, // req_type 
    &type,           // actual_type_return 
    &format,         // actual_format_return 
    &m_nitems,       // nitems_return 
    &size,           // bytes_after_return 
    &properties      // prop_return
  );

  LOG_DEBUG_D( m_type );
  LOG_DEBUG_D( m_nitems );
  LOG_DEBUG_D( format );

  if( m_dumpedFlag )
  {
    std::vector< uint8_t > data;
    const auto typeStr = xtools::getAtomName( m_type, m_display );
    const auto lenType = ( typeStr.size() + 1 ) * sizeof( char );
    const auto lenFormat = sizeof( int );
    const auto lenItems = sizeof( unsigned long );
    data.resize( lenType + lenFormat + lenItems );
    memcpy( data.data(), typeStr.c_str(), lenType );
    memcpy( data.data() + lenType, &format, lenFormat );
    memcpy( data.data() + lenType + lenFormat, &m_nitems, lenItems );
    m_interface->onWrite( data.data(), data.size() );
  }
  else if( m_formatedFlag )
  {
    std::stringstream sfmt;
    sfmt << "type: " << xtools::getAtomName( m_type, m_display ) << std::endl
         << "format: " << format << std::endl
         << "items: " << m_nitems << std::endl
         << "data: ";
    std::string dfmt = sfmt.str();
    m_interface->onWrite( dfmt.data(), dfmt.size() );
  }

  // get size
  m_size = m_nitems * xtools::getItemSize( format );

  LOG_DEBUG_D( m_size );

  if( m_type == XA_ATOM )
  {
    LOG_DEBUG( "print as atoms" );
    const Atom* atoms = reinterpret_cast< const Atom* >( properties );
    for( auto i = 0; i < m_nitems; i++ )
    {
      std::string name = xtools::getAtomName( atoms[ i ], m_display );
      if( !m_dumpedFlag )
        name += "\n";
      const auto sz = name.size();
      m_interface->onWrite( name.c_str(), ( m_dumpedFlag ? sz + 1 : sz ) * sizeof( char ) );
    }
  }
  else if( m_type == XA_INTEGER )
  {
    LOG_DEBUG( "print as integers" );
    const long* longs = reinterpret_cast< const long* >( properties );
    if( m_dumpedFlag )
    {
      m_interface->onWrite( longs, m_size );
    }
    else for( auto i = 0; i < m_nitems; i++ )
    {
      std::stringstream ss;
      ss << longs[ i ] << "\n";
      auto str = ss.str();
      m_interface->onWrite( str.c_str(), str.size() );
    }
  }
  else
  {
    LOG_DEBUG( "print as raw data" );
    m_interface->onWrite( properties, m_size );
  }

  XFree( properties );
}

void Xclient::onIncr()
{
}

void Xclient::onSelectionRequest( const XEvent& event )
{
  LOG_POINT;

  Window requestor = event.xselectionrequest.requestor;
  Atom reqProperty = event.xselectionrequest.property;
  Atom reqTarget = event.xselectionrequest.target;

  const auto name = xtools::getAtomName( reqTarget, m_display );
  const char* format;
  size_t bytes = m_interface->onRead( nullptr, &format, name.c_str(), 0, 0 );

  XEvent res;
  if( format )
  {
    Bytes data( bytes );
    m_interface->onRead( data.data(), &format, name.c_str(), bytes, 0 );

    int bits;
    Atom type;
    int elements;

    if( strcmp( format, "ATOM" ) == 0 )
    {
      LOG_DEBUG( "formatting as atoms" );
      bits = 32;
      type = XA_ATOM;
      data = readAtoms( data.data(), data.size() );
      bytes = data.size();
    }
    else if( strcmp( format, "INTEGER" ) == 0 )
    {
      LOG_DEBUG( "formatting as integers" );
      bits = 32;
      type = XA_INTEGER;
      data = readIntegers( data.data(), data.size() );
      bytes = data.size();
    }
    else
    {
      bits = 8;
      type = reqTarget;
      elements = bytes;
    }

    LOG_DEBUGF( "data size: %d", data.size() );
    LOG_DEBUGF( "bytes: %d", bytes );
    LOG_DEBUGF( "bits: %d", bits );
    LOG_DEBUGF( "sz: %d", bytes / 8 ); //xtools::getItemSize( bits ) );
    LOG_DEBUGF( "sz long: %d", sizeof( long ) );

    XChangeProperty(
      m_display,
      requestor,
      reqProperty,
      type,
      bits,
      PropModeReplace,
      data.data(),
      bytes / xtools::getItemSize( bits )
    );

    res.xselection.property = reqProperty;
  }
  else
  {
    res.xselection.property = None;
  }

  res.xselection.type = SelectionNotify;
  res.xselection.display = event.xselectionrequest.display;
  res.xselection.requestor = requestor;
  res.xselection.selection = event.xselectionrequest.selection;
  res.xselection.target = reqTarget;
  res.xselection.time = event.xselectionrequest.time;

  XSendEvent( m_display, requestor, 0, 0, &res );
  XFlush( m_display );
}

Bytes Xclient::readAtoms( const void* data, size_t size ) const
{
  LOG_POINT;
  Bytes result;
  std::list< Atom > atoms;
  const char* cur = static_cast< const char* >( data );
  while( size > 0 )
  {
    auto len = strnlen( cur, size );
    std::string name( cur, len );
    cur += len;
    size -= len;
    auto atom = xtools::getAtomByName( name, m_display );
    LOG_DEBUG_D( atom );
    atoms.push_back( atom );
    // zero separation
    if( size > 0 )
    {
      cur++;
      size--;
    }
  }
  result.resize( atoms.size() * sizeof( Atom ) );
  Atom* curAtom = reinterpret_cast< Atom* >( result.data() );
  for( const auto& atom : atoms )
  {
    *curAtom = atom;
    curAtom++;
  }
  return result;
}

Bytes Xclient::readIntegers( const void* data, size_t size ) const
{
  Bytes result;
  std::list< long > longs;
  const char* cur = static_cast< const char* >( data );
  while( size > 0 )
  {
    auto len = strnlen( cur, size );
    std::string str( cur, len );
    cur += len;
    size -= len;
    longs.push_back( atol( str.c_str() ) );
    // zero separation
    if( size > 0 )
    {
      cur++;
      size--;
    }
  }
  result.resize( longs.size() * sizeof( long ) );
  long* curLong = reinterpret_cast< long* >( result.data() );
  for( const auto& l : longs )
  {
    *curLong = l;
    curLong++;
  }
  return result;
}

Atom Xclient::getSelection()
{
  LOG_POINT;
  if( !m_selection )
  {
    m_selection = getClipboardAtom();
  }
  return m_selection;
}

Atom Xclient::getClipboardAtom()
{
  LOG_POINT;
  if( !m_clipboardAtom )
  {
    m_clipboardAtom = XInternAtom( m_display, "CLIPBOARD", False );
  }
  return m_clipboardAtom;
}

Atom Xclient::getTargetsAtom()
{
  LOG_POINT;
  if( !m_targetsAtom )
  {
    m_targetsAtom = XInternAtom( m_display, "TARGETS", False );
  }
  return m_targetsAtom;
}

Atom Xclient::getXclimAtom()
{
  LOG_POINT;
  if( !m_xclimAtom )
  {
    m_xclimAtom = XInternAtom( m_display, "XCLIM_OUT", False );
  }
  return m_xclimAtom;
}

Atom Xclient::getIncrAtom()
{
  LOG_POINT;
  if( !m_incrAtom )
  {
    m_incrAtom = XInternAtom( m_display, "INCR", False );
  }
  return m_incrAtom;
}
