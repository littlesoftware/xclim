#pragma once

#include "IClient.h"

#include <map>
#include <list>
#include <string>

class Target;
class Xclient;

class Manager : public IClient
{
public:
  Manager();
  virtual ~Manager();

  void setDryMode();
  void setExtraMode();

  void add( Target&& target );
  void setMemoryLimit( const size_t bytes );

  void onCopy();
  void onPaste();
  void onList();

  virtual void onWrite( const void* data, const size_t size ) override;
  virtual size_t onRead(
      void* data,
      const char** format,
      const char* name,
      const size_t size,
      const size_t pos ) override;
  virtual size_t onReadDump( void* data, const size_t size, const size_t pos ) override;
  virtual void onCreateTarget( const char* name, const char* format ) override;
  virtual void onWriteTarget( const char* name, const void* data, const size_t size ) override;

private:
  using TargetDict = std::map< std::string, Target >;
  using TargetList = std::list< Target >;
  TargetList m_targets;
  TargetDict m_dict;
  Target* m_target;
  Xclient* m_client;
  bool m_dry;
  bool m_extra;
  size_t m_limit;
};
