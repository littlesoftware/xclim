#include "Manager.h"
#include "Target.h"
#include "log.h"
#include "tools.h"
#include "xtools.h"
#include "Xclient.h"

#include <iostream>
#include <sstream>
#include <cstring>

Manager::Manager():
  m_dry( false ),
  m_limit( 1024 * 1024 ), // 1 Mb
  m_extra( false )
{
}

Manager::~Manager()
{
}

void Manager::setDryMode()
{
  m_dry = true;
}

void Manager::setExtraMode()
{
  m_extra = true;
}

void Manager::add( Target&& target )
{
  m_targets.push_back( std::move( target ) );
  LOG_DEBUG_D( m_targets.size() );
}

void Manager::setMemoryLimit( const size_t bytes )
{
  m_limit = bytes;
}

void Manager::onCopy()
{
  LOG_POINT;

  Xclient client;
  client.setInterface( this );
  m_client = &client;
  client.open();

  // move from list into dictionary
  for( auto& target : m_targets )
  {
    if( target.isReference() )
      continue;

    if( target.isDump() )
    {
      m_target = &target;
      target.openIn();
      client.parseDump();
      target.close();
    }
    else
    {
      target.openIn();
      target.saveIn( m_limit );
      target.close();

      m_dict.insert( { target.getName(), std::move( target ) } );
    }
  }

  // add extend targets
  const char* Targets = "TARGETS";
  if( m_dict.find( Targets ) == m_dict.end() )
  {
    std::stringstream ss;
    Target target;
    target.setName( Targets );
    target.setFormat( "ATOM" );
    ss << Targets;
    for( const auto& [ name, _ ] : m_dict )
    {
      ss << '\0' << name;
      LOG_DEBUGF( "targets: %s", name.c_str() );
    }
    const std::string data = ss.str();
    const auto size = data.size() * sizeof( char );
    target.openIn();
    target.saveIn( data.c_str(), size, m_limit );
    target.close();
    m_dict.insert( { Targets, std::move( target ) } );
  }

  // bind client
  m_client->setFormated( m_extra );

  client.bind();

  client.close();
  m_client = nullptr;
}

void Manager::onPaste()
{
  LOG_POINT;
  Xclient client;
  client.setInterface( this );
  m_client = &client;
  client.open();
  client.setFormated( m_extra );
  for( auto& target : m_targets )
  {
    m_target = &target;
    target.openOut();
    client.getTarget( target.getName() );
    target.close();
  }
  client.close();
  m_client = nullptr;
}

void Manager::onList()
{
  LOG_POINT;
  if( m_dry )
  {
    std::cout << "Target list" << std::endl;
    return;
  }
}

void Manager::onWrite( const void* data, const size_t size )
{
  m_target->write( data, size );
}

size_t Manager::onRead(
    void* data,
    const char** format,
    const char* name,
    const size_t size,
    const size_t pos )
{
  // target is not available
  auto it = m_dict.find( name );
  if( it == m_dict.end() )
  {
    *format = nullptr;
    return 0;
  }
  const auto* target = &( it->second );
  // is target referenced
  while( target->isReference() )
  {
    target = &( m_dict.find( target->getReference() )->second );
  }
  // get format of target
  *format = target->getFormat().c_str();
  // if data is null then return size of target
  if( data == nullptr )
    return target->getSize();
  // copy
  return target->read( data, pos, size );
}

size_t Manager::onReadDump( void* data, const size_t size, const size_t pos )
{
  return m_target->read( data, pos, size );
}

void Manager::onCreateTarget( const char* name, const char* format )
{
  Target target;
  target.setName( name );
  target.setFormat( format );
  m_dict.insert( { target.getName(), std::move( target ) } );
}

void Manager::onWriteTarget( const char* name, const void* data, const size_t size )
{
  m_dict.at( name ).saveIn( data, size, m_limit );
}

