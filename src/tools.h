#pragma once

#include "types.h"

#include <stdint.h>
#include <string>

namespace tools
{

std::string DataToHex( const void* data, const size_t len );
std::string BytesToHex( const Bytes& data );
size_t GetMemorySize( const char* str );

} // namespace tools
