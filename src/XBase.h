#pragma once

#include <X11/Xlib.h>

class XBase
{
public:
  XBase();
  ~XBase();

  void open();
  void close();

protected:
  Display* m_display;
  Window m_root;
  Window m_window;
  Atom m_selection;
};
