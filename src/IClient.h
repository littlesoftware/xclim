#pragma once

#include <cstdlib>

class IClient
{
public:
  virtual ~IClient() = default;

  /**
   * Write some data into abstract pipeline
   *
   * @param data is pointer of data for writing
   * @param size is count bytes of data for writing
   */
  virtual void onWrite( const void* data, const size_t size ) = 0;

  /**
   * Read some data from abstract pipeline
   *
   * @param data is pointer where will be write data
   * @param format is platform specific format data. By default using as bytes
   * @param name is target name what need read
   * @param size is maximum bytes avalable to write data
   * @param pos is start position where need reading
   * @return bytes readed into data
   * @note if data is nullptr, then return amount bytes of target
   * @note if format return nullptr, then target is not exists
   */
  virtual size_t onRead(
      void* data,
      const char** format,
      const char* name,
      const size_t size,
      const size_t pos ) = 0;

  /**
   * Read dump data
   *
   * @param data is pointer where will be write data
   * @param size is maximum bytes avalable to write data
   * @param pos is start position where need reading
   * @return bytes readed into data
   */
  virtual size_t onReadDump( void* data, const size_t size, const size_t pos ) = 0;

  /**
   * Invoke create target when reading dump data
   *
   * @param name is target name
   * @param format is target human format
   */
  virtual void onCreateTarget( const char* name, const char* format ) = 0;

  /**
   * Append data of dump
   *
   * @param name is target name. Target is created early
   * @param data is target data
   * @param size is size of target data
   */
  virtual void onWriteTarget( const char* name, const void* data, const size_t size ) = 0;
};
