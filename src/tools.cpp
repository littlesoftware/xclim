#include "tools.h"

#include <X11/Xatom.h>
#include <X11/Xdefs.h>
#include <X11/Xlib.h>

#include <sstream>
#include <stdint.h>
#include <iomanip>
#include <map>
#include <cstring>

namespace tools
{

std::string DataToHex( const void* data, const size_t len )
{
  std::stringstream os;
  const uint8_t* byte = static_cast< const uint8_t* >( data );
  os.fill( '0' );
  for( size_t i = 0; i < len; i++, byte++ )
  {
    if( i != 0 )
    {
      if( i % 16 == 0 )
        os << std::endl;
      else
        os << " ";
    }
    os << std::setw( 2 ) << std::hex << static_cast< int >( *byte );
  }
  return os.str();
}

std::string BytesToHex( const Bytes& data )
{
  return DataToHex( data.data(), data.size() );
}

size_t GetMemorySize( const char* str )
{
  const auto len = strlen( str );
  const char* sym = str + len - 1;
  size_t result;
  if( std::isdigit( *sym ) )
  {
    result = std::stoul( str );
  }
  else
  {
    result = std::stoul( std::string( str, len - 1 ) );
    switch( *sym )
    {
    case 'M':
    case 'm':
      result *= 1024;
    case 'K':
    case 'k':
      result *= 1024;
    }
  }
  return result;
}

} // namespace tools
