#pragma once

#define LOG_ENABLE

#ifdef LOG_ENABLE

#include <cstdio>
#include <cstdarg>

enum class __log_type__
{
  Debug = 0,
  Info,
  Error,
  Off
};

static FILE* __log_file__ = stderr;
static __log_type__ __log_level__ = __log_type__::Off;

inline void __log_write__( __log_type__ type, const char* msg )
{
  static const char* types[] = { "Debug", "Info", "Error" };
  if( type >= __log_level__ )
    fprintf( __log_file__, "%s: %s\n", types[ static_cast< int >( type ) ], msg );
}

inline void __log_format__( __log_type__ type, const char* msg, ... )
{
  va_list args;
  va_start( args, msg );

  // length
  va_list copy;
  va_copy( copy, args );
  const int length = std::vsnprintf( nullptr, 0, msg, copy ) + 1;
  va_end( copy );

  char* data = new char[ length ];

  std::vsnprintf( data, length, msg, args );
  __log_write__( type, data );

  delete[] data;
}

#define LOG_SET_FILE(file) __log_file__ = file;
#define LOG_SET_DEBUG __log_level__ = __log_type__::Debug;
#define LOG_POINT __log_format__( __log_type__::Debug, "%s: %d", __PRETTY_FUNCTION__, __LINE__ );
#define LOG_DEBUG(msg) __log_write__( __log_type__::Debug, msg );
#define LOG_INFO(msg) __log_write__( __log_type__::Info, msg );
#define LOG_ERROR(msg) __log_write__( __log_type__::Error, msg );
#define LOG_DEBUGF(msg, ...) __log_format__( __log_type__::Debug, msg __VA_OPT__(,) __VA_ARGS__ );
#define LOG_INFOF(msg, ...) __log_format__( __log_type__::Info, msg __VA_OPT__(,) __VA_ARGS__ );
#define LOG_ERRORF(msg, ...) __log_format__( __log_type__::Error, msg __VA_OPT__(,) __VA_ARGS__ );
#define LOG_DEBUG_D(var) __log_format__( __log_type__::Debug, #var"=%d", var );
#define LOG_DEBUG_S(var) __log_format__( __log_type__::Debug, #var"='%s'", var );

#else

#define LOG_SET_FILE(file)
#define LOG_SET_DEBUG
#define LOG_POINT
#define LOG_DEBUG(msg)
#define LOG_INFO(msg)
#define LOG_ERROR(msg)
#define LOG_DEBUGF(msg, ...)
#define LOG_INFOF(msg, ...)
#define LOG_ERRORF(msg, ...)
#define LOG_DEBUG_D(var)
#define LOG_DEBUG_S(var)

#endif // LOG_ENABLE
