#include "Handler.h"
#include "Xclient.h"
#include "log.h"
#include "Target.h"
#include "tools.h"

#include <iostream>
#include <stdexcept>
#include <getopt.h>
#include <unistd.h>
#include <cstring>
#include <sstream>
#include <iterator>

const char* Handler::Targets = "TARGETS";

Handler::Handler():
  m_action( Action::Unknown )
{
}

Handler::~Handler()
{
}

int Handler::execute( int argc, char** argv )
{
  Handler& handler = instance();
  try
  {
    handler.parseOptions( argc, argv );
    switch( handler.getAction() )
    {
    case Action::Help:
      handler.onHelp();
      break;
    case Action::Version:
      handler.onVersion();
      break;
    case Action::Copy:
      handler.onCopy();
      break;
    case Action::Paste:
      handler.onPaste();
      break;
    case Action::List:
      handler.onList();
      break;
    default:
      std::cerr << "Unknown action "
                << static_cast< int >( handler.getAction() ) << std::endl;
      return -1;
    }
  }
  catch( const std::exception& error )
  {
    std::cerr << "Error: " << error.what() << std::endl;
    return -2;
  }
  catch( int code )
  {
    return code;
  }
  catch( ... )
  {
    std::cerr << "Unknown error" << std::endl;
    return -3;
  }
  return 0;
}

Handler& Handler::instance()
{
  static Handler obj;
  return obj;
}

void Handler::parseOptions( int argc, char** argv )
{
  LOG_POINT
  const option longOptions[] = {
    { "output", no_argument, nullptr, 'o' },
    { "paste", no_argument, nullptr, 'o' },
    { "input", no_argument, nullptr, 'i' },
    { "copy", no_argument, nullptr, 'i' },
    { "yank", no_argument, nullptr, 'i' },
    { "list", no_argument, nullptr, 'l' },
    { "targets", no_argument, nullptr, 'l' },
    { "extra", no_argument, nullptr, 'E' },
    { "dump", no_argument, nullptr, 'U' },
    { "target", required_argument, nullptr, 't' },
    { "data", required_argument, nullptr, 'd' },
    { "file", required_argument, nullptr, 'f' },
    { "in-memory", no_argument, nullptr, 'M' },
    { "in-file", no_argument, nullptr, 'F' },
    { "memory-limit", required_argument, nullptr, 'B' },
    { "and", no_argument, nullptr, 'a' },
    { "debug", no_argument, nullptr, 'D' },
    { "dry", no_argument, nullptr, 'Z' },
    { "help", no_argument, nullptr, 'h' },
    { "version", no_argument, nullptr, 'v' },
    { nullptr, 0, nullptr, 0 }
  };
  const char shortOptions[] = "oiylEUt:d:f:MFB:aDhvZ";
  int name;
  int argind;
  m_action = Action::Unknown;
  Target target;
  std::string targetName;

  while( ( name = getopt_long( argc, argv, shortOptions, longOptions, nullptr ) ) != -1 )
  {
    switch( name )
    {
    case 'o':
      m_action = Action::Paste;
      break;
    case 'y':
    case 'i':
      m_action = Action::Copy;
      break;
    case 'l':
      m_action = Action::List;
      break;
    case 'E':
      m_manager.setExtraMode();
      break;
    case 'U':
      target.setDump();
      break;
    case 't':
      target.setName( optarg );
      break;
    case 'd':
      // support read data from arg value
      target.setData( optarg, strlen( optarg ) * sizeof( char ) );
      break;
    case 'f':
      // support read data from file
      target.setFile( optarg );
      break;
    case 'M':
      // should be storage in memory (ignore memory limit)
      target.storeInMemory();
      break;
    case 'F':
      // should be storage in file (ignore memory limit)
      target.storeInCache();
      break;
    case 'B':
      // memory limit
      m_manager.setMemoryLimit( tools::GetMemorySize( optarg ) );
      break;
    case 'a':
      // support multi targets. It is separator of targets
      targetName = target.getName();
      m_manager.add( std::move( target ) );
      target.setReference( targetName ); // new target is referenced on previously target
      break;
    case 'D':
      LOG_SET_DEBUG;
      break;
    case 'Z':
      m_manager.setDryMode();
      break;
    case 'h':
      m_action = Action::Help;
      break;
    case 'v':
      m_action = Action::Version;
      break;
    case '?':
      throw 1;
      break;
    }
  }

  if( m_action == Action::Unknown )
  {
    m_action = isatty( 0 ) ? Action::Paste : Action::Copy;
  }
  LOG_DEBUG_D( m_action )

  argind = optind;
  if( argind < argc )
  {
    for( ; argind < argc; argind++ )
    {
      target.setName( argv[ argind ] );
      m_manager.add( std::move( target ) );
      target.setReference( argv[ argind ] );
      LOG_DEBUG_S( argv[ argind ] );
    }
  }
  else
  {
    m_manager.add( std::move( target ) );
  }
}

Handler::Action Handler::getAction() const
{
  return m_action;
}

void Handler::onHelp()
{
  LOG_POINT;
}

void Handler::onVersion()
{
  LOG_POINT;
}

void Handler::onCopy()
{
  m_manager.onCopy();
}

void Handler::onPaste()
{
  m_manager.onPaste();
}

void Handler::onList()
{
  m_manager.onList();
}

