#include "Target.h"
#include "tools.h"
#include "log.h"

#include <cstring>
#include <stdio.h>
#include <unistd.h>
#include <memory>
#include <cassert>
#include <stdio.h>

Target::Target()
{
  reset();
}

Target::Target( Target&& obj ):
  m_name( std::move( obj.m_name ) ),
  m_format( std::move( obj.m_format ) ),
  m_data( std::move( obj.m_data ) ),
  m_file( obj.m_file ),
  m_cache( obj.m_cache ),
  m_filename( std::move( obj.m_filename ) ),
  m_storeInMemory( obj.m_storeInMemory ),
  m_storeInCache( obj.m_storeInCache ),
  m_isDump( obj.m_isDump ),
  m_storeInReference( obj.m_storeInReference ),
  m_reference( std::move( obj.m_reference ) )
{
  obj.reset();
}

Target::~Target()
{
  if( m_cache )
  {
    fclose( m_cache );
    m_cache = nullptr;
  }
}

void Target::setName( const std::string& name )
{
  m_name = name;
}

const std::string& Target::getName() const
{
  return m_name;
}

void Target::setFormat( const std::string& format )
{
  m_format = format;
}

const std::string& Target::getFormat() const
{
  return m_format;
}

void Target::setData( const void* data, const size_t size )
{
  m_data.resize( size );
  memcpy( m_data.data(), data, size );
  m_storeInReference = false;
}

void Target::setFile( const char* filename )
{
  m_filename = filename;
  m_storeInReference = false;
}

void Target::storeInMemory()
{
  m_storeInMemory = true;
  m_storeInCache = false;
}

void Target::storeInCache()
{
  m_storeInMemory = false;
  m_storeInCache = true;
}

void Target::setDump()
{
  m_isDump = true;
}

bool Target::isDump() const
{
  return m_isDump;
}

bool Target::isReference() const
{
  return m_storeInReference;
}

void Target::setReference( const std::string& target )
{
  m_storeInReference = true;
  m_reference = target;
}

const std::string& Target::getReference() const
{
  return m_reference;
}

void Target::openIn()
{
  assert( m_file == nullptr );
  if( m_filename.empty() || m_filename == "-" )
  {
    m_file = stdin;
    return;
  }
  m_file = fopen( m_filename.c_str(), "rb" );
}

void Target::openOut()
{
  assert( m_file == nullptr );
  if( m_filename.empty() || m_filename == "-" )
  {
    m_file = stdout;
    return;
  }
  m_file = fopen( m_filename.c_str(), "wb" );
}

void Target::close()
{
  if( m_file && m_file != stdout && m_file != stdin )
  {
    fclose( m_file );
  }
  m_file = nullptr;
}

void Target::saveIn( const size_t limit )
{
  LOG_POINT;
  m_size = 0;
  // read from buffered data
  if( !m_data.empty() )
  {
    LOG_POINT;
    m_size = m_data.size();
    // save in cache
    if( m_storeInCache )
    {
      // save in cache all
      m_cache = tmpfile();
      fwrite( m_data.data(), 1, m_size, m_cache );
      m_data.clear();
    }
    else if( !m_storeInMemory && ( m_size > limit ) )
    {
      // save in cache extra data
      m_cache = tmpfile();
      fwrite( m_data.data() + limit, 1, m_size - limit, m_cache );
      m_data.resize( limit );
    }
    return;
  }
  // read from file
  assert( m_file );
  assert( m_file != stdout );
  LOG_POINT;

  if( m_storeInCache )
  {
    m_cache = tmpfile();
  }

  const size_t bfs = 1024;
  uint8_t buffer[ bfs ];
  while( !feof( m_file ) )
  {
    LOG_DEBUG_D( bfs );
    auto bytes = fread( buffer, 1, bfs, m_file );
    LOG_DEBUG_D( bytes );
    if( bytes <= 0 )
      continue;
    saveIn( buffer, bytes, limit );
  }
}

void Target::saveIn( const void* data, const size_t size, const size_t limit )
{
  m_size += size;
  const uint8_t* buffer = static_cast< const uint8_t* >( data );

  LOG_DEBUG_D( m_size );

  if( m_storeInMemory )
  {
    const auto old = m_data.size();
    m_data.resize( old + size );
    memcpy( m_data.data() + old, buffer, size );
  }
  else if( m_cache )
  {
    fwrite( buffer, 1, size, m_cache );
  }
  else
  {
    const auto old = m_data.size();
    if( old >= limit )
    {
      m_cache = tmpfile();
      fwrite( buffer, 1, size, m_cache );
    }
    else if( old + size > limit )
    {
      m_cache = tmpfile();
      m_data.resize( limit );
      const auto toBuff = limit - old;
      memcpy( m_data.data() + old, buffer, toBuff );
      fwrite( buffer + toBuff, 1, size - toBuff, m_cache );
    }
    else
    {
      m_data.resize( old + size );
      memcpy( m_data.data() + old, buffer, size );
    }
  }
}

size_t Target::read( void* data, const size_t pos, const size_t size ) const
{
  LOG_POINT;
  size_t bytes = pos + size > m_size ? m_size - pos : size;
  LOG_DEBUG_D( bytes );
  if( bytes <= 0 )
    return 0;
  if( m_storeInMemory )
  {
    LOG_POINT;
    memcpy( data, m_data.data(), bytes );
    return bytes;
  }
  if( m_storeInCache )
  {
    LOG_POINT;
    fseek( m_cache, pos, SEEK_SET );
    return fread( data, 1, bytes, m_cache );
  }
  LOG_POINT;
  const auto sz = m_data.size();
  const size_t memSz = pos >= sz ? 0 : std::min( sz - pos, bytes );
  const size_t cacheSz = bytes - memSz;
  LOG_DEBUG_D( sz );
  LOG_DEBUG_D( memSz );
  LOG_DEBUG_D( cacheSz );
  // read from memory
  if( memSz > 0 )
  {
    memcpy( data, m_data.data() + pos, memSz );
  }
  // read from cache
  if( cacheSz > 0 )
  {
    fseek( m_cache, pos - ( sz - memSz ), SEEK_SET );
    fread( static_cast< uint8_t* >( data ) + memSz, 1, cacheSz, m_cache );
  }
  return bytes;
}

size_t Target::getSize() const
{
  return m_size;
}

void Target::reset()
{
  m_name = "UTF8_STRING";
  m_format.clear();
  m_data.clear();
  m_filename = "-";
  m_file = nullptr;
  m_cache = nullptr;
  m_storeInMemory = false;
  m_storeInCache = false;
  m_isDump = false;
  m_storeInReference = false;
  m_reference.clear();
}

void Target::write( const void* data, const size_t size )
{
  assert( m_file );
  fwrite( data, size, 1, m_file );
}

